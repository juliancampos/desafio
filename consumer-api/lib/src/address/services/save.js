const { address: model } = require('../../../database/models');

module.exports = async (repository, values) => {
  const result = await repository.create(model, values)
  return result;
}