const sqs = require('../../../clients/aws/sqs');

const send = (queue, data) => sqs.send(queue, data);

const learningQueue = (data) => {
  const queue = 'learning';
  send(queue, data.person);
}

const analysisQueue = (data) => {
  const queue = 'analysis';
  send(queue, data.address);
}

module.exports = (data) => {
  learningQueue(data);
  analysisQueue(data);
}