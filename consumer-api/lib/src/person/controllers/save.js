const personService = require('../services');
const addressService = require('../../address/services');
const debitService = require('../../debit/services');
const intelligence = require('../../intelligence/services/send');
const repository = require('../../../database/repository');
const sqs = require('../../../clients/aws/sqs');

module.exports = async (req, res) => {
  try {
    const { person, address, debit } = req.body;

    const personData = await personService.save(repository, person);

    address.personId = personData.id;
    await addressService.save(repository, address);

    debit.personId = personData.id;
    await debitService.save(repository, debit);
    
    intelligence(req.body); // sqs.send('aprendizagem', { message: 'primeira mensagem de aprendizagem' });

    return res.status(200).send(personData);
  } catch (error) {
    return res.status(500).send(`Error: ${error.message}`);
  }
}