const { person: model, address, debit } = require('../../../database/models');

const createQuery = (values) => ({ where: values, include: [{ model: address }, { model: debit }] });

module.exports = async (repository, values = {}) => {
  const result = await repository.findAll(model, createQuery(values));
  return result;
}